#define MAX_SIZE (20)

#define SS_S4F   (GPIO_PIN_4) // on port A
#define SCLK_S4F (GPIO_PIN_5) // on port A
#define MISO_S4F (GPIO_PIN_6) // on port A
#define MOSI_S4F (GPIO_PIN_7) // on port A
#define LED_X    (GPIO_PIN_13) //on port B
#define LED_Y    (GPIO_PIN_14) //on port B
#define LED_Z    (GPIO_PIN_15) //on port B

void Init_S4F_SPI(void);
void GPIO_Init_SPI1(void);
void SPI1_Init(void);
void spi_transmit();
void spi_receive();

