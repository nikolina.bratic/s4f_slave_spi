#include "main.h"
#include "SPI.h"

SPI_HandleTypeDef hspi1;
uint8_t rxbuf[MAX_SIZE]={0};
uint8_t txbuf[2]={1,2};

void Init_S4F_SPI(void)
{
	SPI1_Init();
	GPIO_Init_SPI1();

}

void SPI1_Init(void)
{

  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_SLAVE;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }

}

void GPIO_Init_SPI1(void)
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOH_CLK_ENABLE();
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOB_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(GPIOB, LED_X|LED_Y|LED_Z, GPIO_PIN_SET);

	  /*Configure GPIO pins : LED_X LED_Y LED_Z */
	  GPIO_InitStruct.Pin = LED_X|LED_Y|LED_Z;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

void spi_receive(){

	//while(HAL_SPI_GetState(&hspi1)!= HAL_SPI_STATE_READY);
	HAL_StatusTypeDef test;
	test = HAL_SPI_Receive(&hspi1, rxbuf,sizeof(rxbuf), 5000);
		if(HAL_SPI_Receive(&hspi1, rxbuf,sizeof(rxbuf), 1000)!=HAL_OK){
			Error_Handler();
			}
			HAL_GPIO_WritePin(GPIOB, LED_Z, GPIO_PIN_RESET);
			HAL_Delay(250);


}

void spi_transmit(){
	HAL_StatusTypeDef test1;
	test1=HAL_SPI_Transmit(&hspi1, txbuf, 2, 1000);
	if(HAL_SPI_Transmit(&hspi1, txbuf, 2, 1000)==HAL_OK){
		HAL_GPIO_WritePin(GPIOB, LED_Y, GPIO_PIN_RESET);
		HAL_Delay(250);
	}
}


